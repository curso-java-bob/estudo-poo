package executavel.poo;

import classes.poo.Aluno;

public class Aula_poo_declaracao_publica_privada {

    /*MAIN E UM METODO AUTO EXECUTAVEL EM JAVA*/
    public static void main(String[] args) {

        /* NEW ALUNO() E UMA INSTANCIA (CRIACAO DE OBJETO) */
        /* ALUNO1 E UMA REFERENCIA PARA O OBJETO ALUNO */

        Aluno aluno1 = new Aluno(); /*AQUI SERA O JOAO*/
//        aluno1.nome = "Joao"; /* ESSA FORMA NUNCA SERA USADA PARA UM OBJETO */
        /*aluno1.idade = 50;   *//* ESSA FORMA NUNCA SERA USADA PARA UM OBJETO */

//        System.out.println(" Nome do aluno 1 e = " + aluno1.nome);
//        System.out.println(" A idade do aluno 1 e = " + aluno1.idade);

        Aluno aluno2 = new Aluno(); /*AQUI SERA O PEDRO*/

        Aluno aluno3 = new Aluno(); /*AQUI SERA O ALEX*/

        Aluno aluno4 = new Aluno("Maria");

        Aluno aluno5 = new Aluno("Jose", 50);


    }


}
