package executavel.poo;

import classes.poo.Aluno;
import classes.poo.Disciplina;

import javax.swing.*;
import javax.swing.plaf.synth.SynthOptionPaneUI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Aula_poo {

    /*MAIN E UM METODO AUTO EXECUTAVEL EM JAVA*/
    public static void main(String[] args) {

        List<Aluno> alunos = new ArrayList<Aluno>();

        for (int qtd = 1; qtd <= 2; qtd++) {

            /* NEW ALUNO() E UMA INSTANCIA (CRIACAO DE OBJETO) */
            /* ALUNO1 E UMA REFERENCIA PARA O OBJETO ALUNO */

            String nome = JOptionPane.showInputDialog("Qual o nome do aluno"  + qtd + " ? ");
        /*String idade = JOptionPane.showInputDialog("Qual a idade do aluno");
        String dataNascimento = JOptionPane.showInputDialog("Qual a data de nascimento do aluno");
        String rg = JOptionPane.showInputDialog("Qual o RG do aluno");
        String cpf = JOptionPane.showInputDialog("Qual o CPF do aluno");
        String nomeMae = JOptionPane.showInputDialog("Qual o nome da mãe do aluno");
        String nomePai = JOptionPane.showInputDialog("Qual o nome do pai do aluno");
        String dataMatricula = JOptionPane.showInputDialog("Qual a data da matricula do aluno");
        String serieMatriculado = JOptionPane.showInputDialog("Qual a serie do aluno");
        String nomeEscola = JOptionPane.showInputDialog("Qual o nome da escola do aluno");*/


        Aluno aluno1 = new Aluno();
        aluno1.setNome(nome);
        /*aluno1.setIdade(Integer.valueOf(idade));
        aluno1.setDataNascimento(dataNascimento);
        aluno1.setRegistroGeral(rg);
        aluno1.setNumeroCpf(cpf);
        aluno1.setNomeMae(nomeMae);
        aluno1.setNomePai(nomePai);
        aluno1.setDataMatricula(dataMatricula);
        aluno1.setSerieMatriculado(serieMatriculado);
        aluno1.setNomeEscola(nomeEscola);*/

        for (int pos = 1; pos <= 4; pos++) {
            String nomeDisciplina = JOptionPane.showInputDialog("Nome da disciplina " + pos + " ?");
            String notaDisciplina = JOptionPane.showInputDialog("Nota da disciplina " + pos + " ?");
            Disciplina disciplina = new Disciplina();
            disciplina.setDisciplina(nomeDisciplina);
            disciplina.setNota(Double.valueOf(notaDisciplina));

            aluno1.getDisciplinas().add(disciplina);


        }

        int escolha = JOptionPane.showConfirmDialog(null, "Deseja remover alguma disciplina ?");

        if (escolha == 0) { /*Sim é zero*/

            int continuarRemover = 0;
            int posicao = 1;

            Disciplina[] bola =  aluno1.getDisciplinas().toArray(new Disciplina[aluno1.getDisciplinas().size()]);

            while (continuarRemover == 0 && bola.length > 0) {


                String disciplinaRemover = JOptionPane.showInputDialog("Qual a disciplina 1, 2, 3, ou 4 ?");
                bola[Integer.parseInt(disciplinaRemover) - posicao] = null;
                continuarRemover = JOptionPane.showConfirmDialog(null, "Continuar a remover ?");

            }
            aluno1.getDisciplinas() = ()Arrays.asList(bola);
        }

        alunos.add(aluno1);

    }

        for (Aluno aluno : alunos) {

            System.out.println(aluno); /*DESCRICAO DO OBJETO NA MEMORIA*/
            System.out.println("Media do aluno = " + aluno.getMediaNota());
            System.out.println("Resultado = " + aluno.getAlunoAprovado2());
            System.out.println("-------------------------------------------------------------------------");
        }


        /*System.out.println("Aluno1 nome = " + aluno1.getNome());
        System.out.println("Aluno1 idade = " + aluno1.getIdade());
        System.out.println("Aluno1 nascimento = " + aluno1.getDataNascimento());
        System.out.println("Aluno1 RG = " + aluno1.getRegistroGeral());
        System.out.println("Aluno1 CPF = " + aluno1.getNumeroCpf());
        System.out.println("Aluno1 Nome da mãe = " + aluno1.getNomeMae());
        System.out.println("Aluno1 Nome da pai = " + aluno1.getNomePai());
        System.out.println("Aluno1 data da matricula = " + aluno1.getDataMatricula());
        System.out.println("Aluno1 serie matriculado = " + aluno1.getSerieMatriculado());
        System.out.println("Aluno1 nome da escola= " + aluno1.getNomeEscola());
        System.out.println("Media da nota = " + aluno1.getMediaNota());
        System.out.println("Resultado = " + (aluno1.getAlunoAprovado() ? "Aprovado" : "Reprovado"));
        System.out.println("Resultado = " + aluno1.getAlunoAprovado2() + "\n");*/





    }


}






